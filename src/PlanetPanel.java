import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
public class PlanetPanel extends JPanel implements MouseListener
{
  public static final int WIDTH = 800;
  public static final int HEIGHT = 540;
  public static final int FPS = 60;
  public final double distanceconversion = 1000;
  public final double timeconversion = 86400;
  public final double G;
  public long timeStarted;
  public SettingsPanel settingspanel;
  World world;

  class Runner implements Runnable
  {
        public void run(){
            while(true){
                world.updatePlanets(1.0 / (double) FPS);
                repaint();
                try{
                    Thread.sleep(1000/FPS);
                }
                catch(InterruptedException e){}
            }
        }
    }
    public PlanetPanel(SettingsPanel sp)
    {
      world = new World(this);
      G = Math.pow(timeconversion, 2) / (Math.pow(10, 11) * 6.67 * Math.pow(distanceconversion, 3));
      this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
      addMouseListener(this);
      settingspanel = sp;
      Thread mainThread = new Thread(new Runner());
      mainThread.start();
    }



    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        world.drawPlanets(g);
    }




     public void mouseClicked(MouseEvent e)
    {
    	 if(world.getNumberOfPlanets() == 0)
    	 {
    		 timeStarted = System.currentTimeMillis();
    	 }
      int xPos = e.getX();
      int yPos = e.getY();
      Vector clickPos = new Vector(xPos, yPos);
      int planetIndex = -1;
      for(int i = 0; i < world.planets.size(); i ++)
      {
        Vector dFromC = world.planets.get(i).position.difference(clickPos);
        if(dFromC.getMagnitude() < world.planets.get(i).radius)
          {

            planetIndex = i;
            break;
          }
      }
      if(planetIndex != -1)
      {
        world.removePlanet(planetIndex);
      }
      else
      {
        Vector pos = new Vector((double) xPos, (double) yPos);
        Vector vel = settingspanel.getVelocityVector().scalarProduct(Math.pow(10,  settingspanel.MetersPowerValue)/distanceconversion);
        double mass = settingspanel.getMass() * Math.pow(10,  settingspanel.MassPowerValue);
        double radius = settingspanel.getRadius() / distanceconversion * (Math.pow(10,  settingspanel.MetersPowerValue));
        if(vel != null && mass > 0 && radius > 0)
        {
          world.addPlanet(pos, vel, mass, radius);
        }

      }

      //System.out.println(xPos + ", " + yPos);


    }

    public void mousePressed(MouseEvent e)
    {
    //  System.out.println("mousePressed");

    }
    public void mouseReleased(MouseEvent e)
    {
    //  System.out.println("mouseReleased");
    }
    public void mouseEntered(MouseEvent e)
    {
    //  System.out.println("mouseEntered");
    }
    public void mouseExited(MouseEvent e)
   {
    //  System.out.println("mouseExited");
    }
 

}

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.awt.*;
public class Vector
{
  double x;
  double y;
  double magnitude;
  double radangle;
  public Vector(double x, double y)
  {
    this.x = x;
    this.y = y;
    magnitude = (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
    if(x>=0&&y<=0){
    	radangle=Math.atan(Math.abs(y)/Math.abs(x));
    }
    else if(x<0&&y<0){
    	radangle=Math.PI-Math.atan(Math.abs(y)/Math.abs(x));
    }
    else if(x<0&&y>=0){
    	radangle=Math.PI+Math.atan(Math.abs(y)/Math.abs(x));
    }
    else{
    	radangle=2*Math.PI-(Math.atan(Math.abs(y)/Math.abs(x)));
    }
  }

  public Vector difference(Vector vector)
  {
    Vector toReturn = new Vector(this.x - vector.x, this.y - vector.y);
    return toReturn;
  }
  public Vector sum(Vector vector)
  {
    Vector toReturn = new Vector(this.x - vector.x, this.y - vector.y);
    return toReturn;
  }

  public double getMagnitude()
  {
    return magnitude;
  }
  public void add(Vector v)
  {
    this.x += v.x;
    this.y += v.y;
    this.magnitude = (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
  }
  public void subtract(Vector v)
  {
    this.x -= v.x;
    this.y -= v.y;
    this.magnitude = (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
  }

  public Vector crossProduct(Vector vector)
  {
    return null;
  }

  public double dotProduct(Vector vector)
  {
    return this.x*vector.x+this.y*vector.y;
  }

  public static Vector calculateAcceleration(Planet planet1, Planet planet2, double G)
  {
      Vector toReturn = new Vector (0, 0);
      Vector distance = planet1.getDistance(planet2);
      toReturn.x += (distance.x * planet2.mass * G) / (Math.pow(distance.getMagnitude(), 3));
      toReturn.y += (distance.y * planet2.mass * G) / (Math.pow(distance.getMagnitude(), 3));
      return toReturn;
  } 
  public Vector scalarProduct(double d)
  {
	  Vector toReturn = new Vector(this.x, this.y);
	  toReturn.x *= d;
	  toReturn.y *= d;
	  return toReturn;
  }
  
  public String toString(){
	  return "" + x +"\n" + y+"\n"+radangle* 180/Math.PI;
  }
}

This program creates planets in space which are gravitationally attracted to
each other. They accelerate according to Newton's law of gravitation. In addition,
collisions between planets follow the physical rules of elastic collisions,
maintaining linear momentum and kinetic energy. Users can enter initial
velocities, radii, and masses, and click to create planets. Interesting
configurations can be created, including orbits. Units are also included which
are proportional to real life, so for Example our solar system could be created.

Users can remove the last planet created with the delete last planet button, or
by clicking on a given planet.

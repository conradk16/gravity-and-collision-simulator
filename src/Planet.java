import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.awt.*;

public class Planet {
  World world;
  Vector position;
  Vector velocity;
  Vector acceleration;
  final double mass;
  final double radius;
  Color color;
  public Planet(Vector pos, Vector vel, double m, double rad, World w)
  {
    position = pos;
    radius = rad;
    velocity = vel;
    acceleration = new Vector(0, 0);
    mass = m;
    world = w;
    color = new Color(255, 255, 255);
  }
  public void calculateInteractions(int i)
  {
    ArrayList<Planet> planets = world.getPlanets();
    Vector newAcceleration = new Vector(0, 0);

    for(int index = 0; index < planets.size(); index++)
    {
        
        
    	if(index != i)
    	{

    		Vector v =  Vector.calculateAcceleration(this, planets.get(index), world.planetpanel.G);
    		newAcceleration.add(v);
    		
    		if(Math.sqrt(Math.pow(this.position.x - planets.get(index).position.x,2)+
    				Math.pow(this.position.y - planets.get(index).position.y,2))<=this.radius+planets.get(index).radius){

    			//unit vector for x coordinate for new axes
    			
    			boolean thisisright;
    			if(this.position.x>planets.get(index).position.x)
    				thisisright=true;
    			else
    				thisisright=false;
    			
    			boolean thisistop;
    			if(this.position.y<planets.get(index).position.y)
    				thisistop=true;
    			else
    				thisistop=false;
    			
    			Vector unit;
    			if(thisisright)
    				unit = new Vector(this.position.x-planets.get(index).position.x, this.position.y-planets.get(index).position.y);
    			else
    				unit = new Vector(planets.get(index).position.x-this.position.x, planets.get(index).position.y-this.position.y);
    			unit.x*=(1/unit.getMagnitude());
    			unit.y*=(1/unit.getMagnitude());

    			//unit vector for y coordinate for new axes
    			
    			Vector perpunit = new Vector(unit.y, -unit.x);
    			
    			//set velocities in new coordinate system
    			
    			double thisyvelocity = this.velocity.dotProduct(perpunit);
    			double otheryvelocity = planets.get(index).velocity.dotProduct(perpunit);

    			double thisxvelocity = this.velocity.dotProduct(unit);
    			double otherxvelocity = planets.get(index).velocity.dotProduct(unit);
    			
    			//the collision

    			double newthisxvelocity = (thisxvelocity*(this.mass-planets.get(index).mass))/(this.mass+planets.get(index).mass) + 
    					(otherxvelocity*2*planets.get(index).mass)/(this.mass+planets.get(index).mass);
    			double newotherxvelocity = (otherxvelocity*(planets.get(index).mass-this.mass))/(this.mass+planets.get(index).mass) + 
    					(thisxvelocity*2*this.mass)/(this.mass+planets.get(index).mass);

    			//post collision vectors in new coordinate system

    			Vector thisvelocity = new Vector(newthisxvelocity, thisyvelocity);
    			Vector othervelocity = new Vector(newotherxvelocity, otheryvelocity);

 
    			//create new unit vector for original x-axis based on new coordinate system
    			
    			Vector unitoriginal;
    			if(thisisright&&!thisistop || (!thisisright&&thisistop))
    				unitoriginal= new Vector(5,5*Math.tan(2*Math.PI-unit.radangle));
    			else if(thisisright&&thisistop || (!thisisright && !thisistop))
    				unitoriginal = new Vector(5,-5*Math.tan(unit.radangle));
    			else{
    				unitoriginal = new Vector(0,0);
    				System.out.println("where are we?");
    			}
    			unitoriginal.x*=(1/unitoriginal.getMagnitude());
    			unitoriginal.y*=(1/unitoriginal.getMagnitude());

    			//create new unit vector for original y-axis

    			Vector unitperporiginal = new Vector(unitoriginal.y,-unitoriginal.x);

    			//reset original velocities

    			double thisfinalxvelocity = thisvelocity.dotProduct(unitoriginal);
    			double thisfinalyvelocity = thisvelocity.dotProduct(unitperporiginal);
    			double otherfinalxvelocity = othervelocity.dotProduct(unitoriginal);
    			double otherfinalyvelocity = othervelocity.dotProduct(unitperporiginal);

    			this.velocity = new Vector(thisfinalxvelocity,thisfinalyvelocity);
    			planets.get(index).velocity = new Vector(otherfinalxvelocity,otherfinalyvelocity);

    			//fix positions

    			Vector unitslope = new Vector(this.position.x-planets.get(index).position.x,this.position.y-planets.get(index).position.y);
    			unitslope.x*=(1/unitslope.getMagnitude());
    			unitslope.y*=(1/unitslope.getMagnitude());

    			this.position.x=planets.get(index).position.x+unitslope.x*(this.radius+planets.get(index).radius);
    			this.position.y=planets.get(index).position.y+unitslope.y*(this.radius+planets.get(index).radius);
    		}
    	}   
   
    }
    acceleration = newAcceleration;
    
  }


  public Vector getDistance(Planet planet)
  {
    Vector distance = planet.position.difference(position);
    return distance;

  }
  public void calculateCollision(Planet planet)
  {

  }
  public void draw(Graphics g)
  {
    
    g.setColor(color);
    g.fillOval((int) (position.x - radius), (int) (position.y - radius), (int) (2*radius), (int) (2*radius));
  
  }

 
  public String toString()
  {
    String toReturn = "Position: (" + position.x + ", " + position.y + ") Velocity: (" + velocity.x + ", "
     + velocity.y + ")  Mass: " + mass + " Radius: " + radius;
     return toReturn;
  }


}
